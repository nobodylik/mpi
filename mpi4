#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <math.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

//----------- задаем основные параметры алгоритма -----------
const int iNX = 256;	 // число узлов по X
const int jNY = 256;	 // число узлов по Y
const int kNZ = 256;     // число узлов по Z

const double a = 100000;	// параметр уравнения (коэф перехода к следующему шагу)
const double EPS = 10e-8;	// порог сходимости

double Dx = 2.0; // размер области по Х
double Dy = 2.0; // размер области по Y
double Dz = 2.0; // размер области по Z

//int L_old = 0;  // номер блока памяти предыдущей итерации
//int L_new = 1;  // номер блока памяти текущей итерации

// набор значений для ускорения расчетов
double hx; // длинна шага по оси X
double hy; // длинна шага по оси Y
double hz; // длинна шага по оси Z
double owy; // длинна шага по Х в квадрате 
double owz; // длинна шага по Y в квадрате 
double owx; // длинна шага по Z в квадрате 
double c;  

double *F_old; // данные предыдущей итераци
double *F_new; // данные текущей итераци

double *buffer_prev; // буфер для соседних значений со стороны меньшего процессора
double *buffer_next; // буфер для соседних значений со стороны большоего процессора
double Fi, Fj, Fk, F1;

int I; // число узлов по X в зоне данного процессора
int J; // число узлов по Y в зоне данного процессора
int K; // число узлов по Z в зоне данного процессора

// double CurMaxDiff; // для тестирования - максимальная разница значений функции в сравнении с предыдущей итерацией
int stop_flag; // равен 0 пока процесс не достиг нужной точности
int tmpf;

// задаем искомую функцию (чтобы с помощью формулы посчитать краевые значения)
double IskomFuncFI(double x, double y, double z) 
{
	return x * x + y * y + z * z;
}

// функция правой части уровнения
double PravFuncRo(double x, double y, double z)  
{
	return  6.0 - a * (x*x + y * y + z * z);
}

// заполняем начальные значения 
void Initit_F(int *lines, int *offset, int rank) 
{  
	for (int i = 0, start = offset[rank]; i < I; ++i, ++start)
		for (int j = 0; j < J; ++j)
			for (int k = 0; k < K; k++)
			{
				if ((start != 0) && (j != 0) && (k != 0) && (start != iNX) && (j != jNY) && (k != kNZ)) 
				{
					// для узлов внутри ставим нули
					F_old[i * J * K + j * K + k] = 0.0;
					F_new[i * J * K + j * K + k] = 0.0;
				}
				else 
				{
					// для узлов на границе ставим значения искомой функции
					F_old[i * J * K + j * K + k] = IskomFuncFI(start * hx, j * hy, k * hz);
					F_new[i * J * K + j * K + k] = IskomFuncFI(start * hx, j * hy, k * hz);
				}
			}
}

// расчет значений следующей итерации для внутренних точек
void Calc_Center_Val(int *lines, int *offsets, int rank) 
{
	for (int i = 1; i < I - 1; ++i)
		for (int j = 1; j < J - 1; j++)
			for (int k = 1; k < K - 1; ++k) 
			{
				Fi = (F_old[(i + 1) * J * K + j * K + k] + F_old[(i - 1) * J * K + j * K + k]) / owx;
				Fj = (F_old[i * J * K + (j + 1) * K + k] + F_old[i * J * K + (j - 1) * K + k]) / owy;
				Fk = (F_old[i * J * K + j * K + (k + 1)] + F_old[i * J * K + j * K + (k - 1)]) / owz;
				F_new[i * J * K + j * K + k] = (Fi + Fj + Fk - PravFuncRo((i + offsets[rank]) * hx, j * hy, k * hz)) / c;
//				if (fabs(F[L1][i * J * K + j * K + k] - IskomFuncFI((i + offsets[rank]) * hx, j * hy, k * hz)) > EPS)
				if (fabs(F_new[i * J * K + j * K + k] - F_old[i * J * K + j * K + k]) > EPS)
					stop_flag = 0;
			}
}

// расчет значений следующей итерации для точек на границе с зоной соседних процессоров
void Calc_NotCenter(int *lines, int *offsets, int rank, int size) 
{
	for (int j = 1; j < J - 1; ++j)
		for (int k = 1; k < K - 1; ++k) 
		{
			if (rank != 0) 
			{                //i = 0
				Fi = (F_old[J * K + j * K + k] + buffer_prev[j * K + k]) / owx;
				Fj = (F_old[(j + 1) * K + k] + F_old[(j - 1) * K + k]) / owy;
				Fk = (F_old[j * K + (k + 1)] + F_old[j * K + (k - 1)]) / owz;
				F_new[j * K + k] = (Fi + Fj + Fk - PravFuncRo(offsets[rank] * hx, j * hy, k * hz)) / c;
//				if (fabs(F[L1][j * K + k] - IskomFuncFI(offsets[rank] * hx, j * hy, k * hz)) > EPS)
				if (fabs(F_new[j * K + k] - F_old[j * K + k]) > EPS)
					stop_flag = 0;
			}
			if (rank != size - 1) 
			{
				int i = lines[rank] - 1;
				Fi = (buffer_next[j * K + k] + F_old[(i - 1) * J * K + j * K + k]) / owx;
				Fj = (F_old[i * J * K + (j + 1) * K + k] + F_old[i * J * K + (j - 1) * K + k]) / owy;
				Fk = (F_old[i * J * K + j * K + (k + 1)] + F_old[i * J * K + j * K + (k - 1)]) / owz;
				F_new[i * J * K + j * K + k] = (Fi + Fj + Fk - PravFuncRo((i + offsets[rank]) * hx, j * hy, k * hz)) / c;
//				if (fabs(F[L1][i * J * K + j * K + k] - IskomFuncFI((i + offsets[rank]) * hx, j * hy, k * hz)) > EPS)
				if (fabs(F_new[i * J * K + j * K + k] - F_old[i * J * K + j * K + k]) > EPS)
					stop_flag = 0;
			}
		}
}

// сравниваем получившиеся значения с правильной функцией, чтобы оценить размер ошибки
void CheckDiff(int *lines, int *offsets, int rank)  
{
	double max = 0.0;
	double tmpM = 0.0;
	for (int i = 1; i < I - 1; i++) 
	{
		for (int j = 1; j < J - 1; j++) 
		{
			for (int k = 1; k < K - 1; k++)
			{
				if ((F1 = fabs(F_new[i * J * K + j * K + k] - IskomFuncFI((i + offsets[rank]) * hx, j * hy, k * hz))) > max)
					max = F1;
			}
		}
	}
	// каждый процессор посчитал размер ошибки для своей зоны. Собираем максимум
	MPI_Allreduce(&max, &tmpM, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD); //

	if (rank == 0) printf("Max differ = %lf\n", tmpM);
}

int main(int argc, char **argv) {
	int size, rank;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int *offsets = (int*)malloc(size * sizeof(int));  //  номер слоя начиная с которого начинается зона данного процессора (по координате X)
	int *lines = (int*)malloc(size * sizeof(int));    //  число слоев, которые входят в зону данного процессора (по координате X) 

	// разбиваем все узлы на зоны, для каждого процессора. Заполняем lines и offsets
	int height = iNX + 1; // общее число узлов по Х
	int tmp = size - (height % size); // если число узлов не делится нацело между процессорами, то считаем число процессоров со стандартной шириной
	for (int i = 0, cur_line = 0; i < size; ++i) {
		lines[i] = (i < tmp) ? (height / size) : (height / size + 1); // для нескольких процессоров в конце списка, ширина полосы будет на 1 больше
		offsets[i] = cur_line;
		cur_line += lines[i];
	}

	// == test output
	if (rank == 0)
	{
		cout << "### " << rank <<" Offse:Lines\n";
		for (int i=0;i<size;i++)
			cout <<i<<" "<< offsets[i] << ":"<< lines[i] << endl;
	}

	I = lines[rank];	// заполняем число узлов по X в зоне данного процессора
	J = jNY + 1;		// заполняем число узлов по Y в зоне данного процессора
	K = kNZ + 1;		// заполняем число узлов по Z в зоне данного процессора

	buffer_prev = (double*)malloc(K * J * sizeof(double)); // создаем буфер для соседних значений со стороны меньшего процессора
	buffer_next = (double*)malloc(K * J * sizeof(double)); // создаем буфер для соседних значений со стороны большего процессора
	F_old = (double*)malloc(I * J * K * sizeof(double)); // предыдущая итерация функции
	F_new = (double*)malloc(I * J * K * sizeof(double)); //текущая итерация функции, будем пересчитывать из одной в другую

	// считаем и сохраняем значения, чтобы не пересчитывать их каждый раз
	hx = Dx / iNX;  
	hy = Dy / jNY;	
	hz = Dz / kNZ;  
	owx = hx * hx;  
	owy = hy * hy;   
	owz = hz * hz;
	c = 2 / owx + 2 / owy + 2 / owz + a;

	Initit_F(lines, offsets, rank); // инициализируем значения F 

	// создаем переменные для асинхронных операций
	MPI_Request send_req_to_next, send_req_to_prev;
	MPI_Request recv_req_from_next, recv_req_from_prev;

	double start = MPI_Wtime();
	int it = 0;
	do {
		it++;
		stop_flag = 1;
//		CurMaxDiff = 99999999;

		if (rank != 0) {
			MPI_Isend(&(F_old[0]), K * J, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &send_req_to_prev); // запуск асинхронной отправки предыдущему процессору
			MPI_Irecv(buffer_prev, K * J, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD, &recv_req_from_prev); // запуск асинхронного получения от предыдущего
		}
		if (rank != size - 1) {
			MPI_Isend(&(F_old[(lines[rank] - 1) * J * K]), K * J, MPI_DOUBLE, rank + 1, 1, MPI_COMM_WORLD, &send_req_to_next); // запуск асинхронной отправки следующему 
			MPI_Irecv(buffer_next, K * J, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &recv_req_from_next); // запуск асинхронного получения от следующего
		}

		Calc_Center_Val(lines, offsets, rank);  // считаем значения следующей итерации для внутренних точек (там где не нужны данные от соседних процессоров)
												
		if (rank != 0) {
			MPI_Wait(&recv_req_from_prev, MPI_STATUS_IGNORE);  // ожидание завершения асинхронной операции
			MPI_Wait(&send_req_to_prev, MPI_STATUS_IGNORE);   // ожидание завершения асинхронной операции
		}
		if (rank != size - 1) {
			MPI_Wait(&recv_req_from_next, MPI_STATUS_IGNORE); // ожидание завершения асинхронной операции
			MPI_Wait(&send_req_to_next, MPI_STATUS_IGNORE); // ожидание завершения асинхронной операции
		}

		// все асинхронные операции завершены, данные для расчета краев лежат в массивах buffer
		Calc_NotCenter(lines, offsets, rank, size); // делаем расчет точек на краю области

		// собираем со всех процессов минимальный flag. Если хоть один процесс передал 0, значит у него нужная точность не достигнута. Продолжаем итерации
		MPI_Allreduce(&stop_flag, &tmpf, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD); 
		stop_flag = tmpf;


		// меняем местами указатели блоков данных (готовимся к следующей итерации)
		double* ptr = F_new; 
		F_new = F_old; 
		F_old = ptr;

	} 
	while (stop_flag == 0);

	// сравниваем полученное значение функции с правильными значениями и печатаем максимальную ошибку
	CheckDiff(lines, offsets, rank);
		
	if (rank==0) 
		printf("Time: %lf\nIterations: %d\n", MPI_Wtime() - start, it);

	// освобождаем память
	free(F_old);
	free(F_new);
	free(buffer_next);
	free(buffer_prev);
	free(lines);
	free(offsets);
	
	MPI_Finalize();

	return 0;
}