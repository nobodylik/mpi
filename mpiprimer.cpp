#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <fstream>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <windows.h>

const double t = 0.01;
const double E = 0.00001;

int N = 5;

double GetA(int i, int j)
{
	if (i == j)
		return 2;
	else
		return 1;
}

double GetB(int i)
{
	return N+1;
}


using namespace std;

//double MyE()
//{
//	double NB = 0;
//	double NBb = 0; // норма
//
//	for (int i = 0; i < N; i++)
//	{
//		NBb = (Bb[i] - B[i])*(Bb[i] - B[i]) + NBb;
//		NB = B[i] * B[i] + NB;
//	}
//
//	NBb = sqrt(NBb);
//	NB = sqrt(NB);
//
//	return NBb / NB;
//}


int main(int argc, char** argv) {
	// Initialize the MPI environment
	MPI_Init(NULL, NULL);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	N = world_size;

	// Get the rank of the process
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int k = world_rank;

	// Get the name of the processor
	/*char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);*/

	double *X = new double[N];
	double *Bb = new double[N];
	double NB = 0;
	double NBb = 0; // норма
	double ee = 1000000;


	double s[1] = {0};

	for(int i=0; i<N;i++)
		X[i] = 0;

	for (int h = 0; h < 10000; h++)
	{
		s[0] = 0;
		for (int j = 0; j < N; j++)//столбец
		{
			s[0] = GetA(k, j) * X[j] + s[0];
		}
		Bb[k] = s[0];

		MPI_Allgather(s, 1, MPI_DOUBLE_PRECISION, Bb, 1, MPI_DOUBLE_PRECISION, MPI_COMM_WORLD);
		NBb = 0;
		NB = 0;
		for (int i = 0; i < N; i++)
		{
			NBb = (Bb[i] - GetB(i))*(Bb[i] - GetB(i)) + NBb;
			NB = GetB(i) * GetB(i) + NB;
		}

		NBb = sqrt(NBb);
		NB = sqrt(NB);

		ee = NBb / NB;

		if (ee < E)
			break;

		for (int i = 0; i < N; i++)
		{
			X[i] = X[i] - t * (Bb[i] - GetB(i));
			//cout << X[i] << " ";
		}
		/*cout << endl;
		cout <<"#"<< h << " e = " << ee << endl;*/
			
	}


	if (k == 0)
	{
		//Sleep(k * 500);
		//std::cout << "number " << k << endl;
		std::cout << "X: ";
		for (int i = 0; i < N; i++)
			std::cout << X[i] << " ";
		//std::cout << endl;
	}
		

	MPI_Finalize();
}