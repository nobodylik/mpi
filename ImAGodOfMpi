#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <fstream>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <time.h> // for clock_gettime


#ifdef _WIN32

#include <intrin.h>
uint64_t rdtsc() {
	return __rdtsc();
}

//  Linux/GCC
#else

uint64_t rdtsc() {
	unsigned int lo, hi;
	__asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
	return ((uint64_t)hi << 32) | lo;
}

#endif

#ifdef _WIN32

#include <windows.h> 
double GetMyTimer()
{

	DWORD t = GetTickCount();
	return t / 1000.0;
}

//  Linux/GCC
#else

double GetMyTimer()
{
	struct timespec MyTime;
	clock_gettime(CLOCK_MONOTONIC_RAW, &MyTime);
	return MyTime.tv_nsec / 1000000000.0 + MyTime.tv_sec;

}
#endif

const double t = 0.01;
const double E = 0.0001;

int N = 100; // размер матрицы

double GetA(int i, int j)
{
	if (i == j)
		return 2;
	else
		return 1;
}

double GetB(int i)
{
	//return N + 1;
	return ((1 + N)*N / 2) + i + 1;  // ответ числа от 1 до N
}

using namespace std;

int main(int argc, char** argv) {
	// Initialize the MPI environment
	MPI_Init(NULL, NULL);

	// Get the number of processes
	int P;
	MPI_Comm_size(MPI_COMM_WORLD, &P);

	// Get the rank of the process
	int k;
	MPI_Comm_rank(MPI_COMM_WORLD, &k);

	
	int NP = 0; // число столбцов в блоке данного процессора
	int NP_Max = 0; // максимальный из всех NP
	int NP_Start = 0; // номер столбца, с которого начинается блок данного процессора = NP_Disp[k]
	int* NP_Arr = new int[P]; // массив со значениями NP для всех процессоров
	int* NP_Disp = new int[P]; // массив со значениями начала блока для i процесса (для функции MPI_Gatherv)


	// заполняем массивы с разбиением матрицы по процессам
	if (N % P == 0)
	{					// число столбцов делится нацело на число процесооров - распределяем их поровну
		for (int i = 0; i < P; i++)
		{
			NP_Arr[i] = N / P;
			NP_Disp[i] = (i* N / P) ;
		}
		NP_Max = N / P;
	}
	else
	{					// число столбцов не делится нацело на число процесооров - распределяем не поровну
		int Ost = N % P;
		for (int i = 0; i < P; i++)
		{
			if (i < Ost)
				NP_Arr[i] = N / P + 1; // у нескольких процессов в начале будет на один столбец больше
			else
				NP_Arr[i] = N / P;

			if (i == 0)
				NP_Disp[i] = 0;
			else
				NP_Disp[i] = NP_Disp[i - 1] + NP_Arr[i - 1] ;
		}
		NP_Max = N / P + 1;
	}

	NP = NP_Arr[k]; // число столбцов в блоке данного процессора
	NP_Start = NP_Disp[k]; // номер столбца, с которого начинается блок данного процессора
	
//	if (k == 0 || k == P - 1) // тестовый вывод - разбивка по блокам каждого процессора
	{
		std::cout << "Start Proc:"<< k << " N:" << N << " P:" << P << " NP=NP_Arr[k]:" << NP << " NP_Disp: " << NP_Disp[k] << std::endl;
	}

	double *X = new double[NP_Max];
	double *forX = new double[NP_Max]; // временный  массив для X
	double *C = new double[NP_Max];
	double *forC = new double[NP_Max]; // массив для сбора С
	double *s = new double[NP_Max]; // частичные суммы
	double *Bb = new double[NP_Max];

	double NB = 0;
	double CSum = 0;
	double NC = 0;
	double NBb = 0; // норма
	double ee = 1000000;
	int TestProcNum = 3;

	std::cout.setf(std::ios::fixed);
	std::cout.precision(2); // n - количество знаков после запятой в выводе

	MPI_Status status;

	NB = 0;  // считаем норму B
	for (int i = 0; i < N; i++)
		NB = GetB(i) * GetB(i) + NB;   
	NB = sqrt(NB);

	for (int i = 0; i<NP; i++)  // начальные значения массива Х 
		X[i] = 0;

	double TimeStart = GetMyTimer();

	for (int h = 0; h < 100000; h++)
	{
		if (k == TestProcNum) // тестовый вывод 
		{
			std::cout << "Proc" << k << " Iter(h):" << h;

			std::cout.precision(8); // n - количество знаков после запятой в выводе
			std::cout << "  e:" << ee << "    X: ";

			std::cout.precision(2); // n - количество знаков после запятой в выводе
			for (int i = 0; i < (NP>15 ? 15 : NP); i++)
				std::cout << i+NP_Start << ":" << X[i] << " ";
			std::cout << std::endl;
		}
		for (int i = 0; i < NP; i++)
			s[i] = 0;
		
		int start_j = NP_Start;

		for (int i = 0; i < P; i++)  // цикл по процессорам (блокам)
		{

			for (int z = 0; z < NP; z++) // цикл по строкам
			{
				for (int j = 0; j < NP_Arr[(k + i) % P]; j++)//столбец
				{
					s[z] = GetA(z + NP_Start, start_j + j) * X[j] + s[z];// собираем частичную сумму Bb
				}
			}
			
			// пересчитываем начало блока для следующей итерации
			start_j += NP_Arr[(k + i) % P];
			start_j = start_j % N;

			MPI_Sendrecv(X, NP_Max, MPI_DOUBLE, (k + P - 1) % P, 0, forX, NP_Max, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

			for (int ii = 0; ii < NP_Max; ii++)
				X[ii] = forX[ii];

		}

		//собираем часть Bb для этого блока
		for (int l = 0; l < NP; l++)
			Bb[l] = s[l];
		

		forC[NP_Max - 1] = 0; 
		for (int z = 0; z < NP; z++)//столбец
			forC[z] = (Bb[z] - GetB(z + NP_Start)) * (Bb[z] - GetB(z + NP_Start));
		
		MPI_Allreduce(forC, C, NP_Max, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	
    	//массив C вспомогательный для расчета числителя из формулы
		CSum = 0;
		for (int i = 0; i < NP_Max; i++)
			CSum = C[i] + CSum;
		
		CSum = sqrt(CSum);
		ee = CSum / NB;

		if (ee < E)  // выход, если добились нужной точности
			break;

		for (int i = 0; i < NP; i++) // np -> mp_max
			X[i] = X[i] - t * (Bb[i] - GetB(i+ NP_Start));
	}

	if (k==P-1) // вывод результата
	{
		std::cout.precision(2);
		std::cout << "Result Proc"<< k << " X: ";
		for (int i = 0; i < (NP>15 ? 15:NP); i++)
			std::cout << i + NP_Start << ":" << X[i] << " ";
		cout << endl;
		double TimeFinish = GetMyTimer();
		double TimeDelta = TimeFinish - TimeStart;
		cout << "Time Sec: "<< TimeDelta << endl;
	}

	MPI_Finalize();
}
